package com.example.bonusservice;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@SpringBootApplication
@EnableConfigurationProperties
@EnableKafkaStreams
public class BonusServiceApplication {

    @Value("${spring.kafka.bootstrap-servers}")
    private String kafkaBootstrapServers;

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${streams.replicationFactor:1}")
    private int streamsReplicationFactor;

    @Value("${streams.applicationServerHost:localhost}")
    private String applicationServerHost;

    @Value("${server.port}")
    private int applicationServerPort;

    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    public KafkaStreamsConfiguration kStreamsConfigs() {
        return new KafkaStreamsConfiguration(
                Map.of(
                        StreamsConfig.APPLICATION_SERVER_CONFIG, applicationServerHost + ":" + applicationServerPort,
                        StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBootstrapServers,
                        StreamsConfig.APPLICATION_ID_CONFIG, applicationName,
                        StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.UUIDSerde.class.getName(),
                        StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName(),
                        StreamsConfig.REPLICATION_FACTOR_CONFIG, streamsReplicationFactor,
                        StreamsConfig.TOPOLOGY_OPTIMIZATION_CONFIG, "all",
                        StreamsConfig.STATE_DIR_CONFIG, "./state",
                        StreamsConfig.EXACTLY_ONCE_V2, true
                )
        );
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.build();
    }
    public static void main(String[] args) {
        SpringApplication.run(BonusServiceApplication.class, args);
    }
}
