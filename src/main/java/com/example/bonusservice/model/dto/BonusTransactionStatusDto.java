package com.example.bonusservice.model.dto;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

public class BonusTransactionStatusDto {

    private UUID from;
    private boolean accepted;
    private ZonedDateTime executionTimestamp;

    public UUID getFrom() {
        return from;
    }

    public void setFrom(UUID from) {
        this.from = from;
    }
    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public ZonedDateTime getExecutionTimestamp() {
        return executionTimestamp;
    }

    public void setExecutionTimestamp(ZonedDateTime executionTimestamp) {
        this.executionTimestamp = executionTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BonusTransactionStatusDto that = (BonusTransactionStatusDto) o;
        return accepted == that.accepted && Objects.equals(from, that.from) && Objects.equals(executionTimestamp, that.executionTimestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, accepted, executionTimestamp);
    }
}
